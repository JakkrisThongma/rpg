package com.company.heroes;

public interface HeroAttributeStrategy
{

    int[] getBaseStats();
    int[] increaseStats();
}
