package com.company.heroes;

public class Mage implements HeroAttributeStrategy
{
    @Override
    public int[] getBaseStats()
    {
        return new int[]{
                100,2,3,10};
    }

    @Override
    public int[] increaseStats()
    {
        return new int[]{15,1,2,5};
    }

}
