package com.company.heroes;

public class Ranger implements HeroAttributeStrategy
{

    @Override
    public int[] getBaseStats() {
        return new int[]{120,5,10,2};
    }

    @Override
    public int[] increaseStats() {
        return new int[]{20,2,5,1};
    }

}
