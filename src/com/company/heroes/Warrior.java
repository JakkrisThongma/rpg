package com.company.heroes;

public class Warrior implements HeroAttributeStrategy
{

    @Override
    public int[] getBaseStats() {
        return new int[]{150,10,3,1};
    }

    @Override
    public int[] increaseStats() {
        return new int[]{30,5,2,1};
    }
}
