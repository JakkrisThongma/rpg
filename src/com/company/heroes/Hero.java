package com.company.heroes;

import com.company.items.*;

public class Hero
{
    public HeroAttributeStrategy hero;
    //Base stats
    private int hp, strength, dexterity, intellect,lvl;

    //Effective stats
    private int efhp, efstr, efdex, efint;
    private int xpReq = 100;
    private int currentXP;

    //Equipment slots
    private Weapon weapon;
    private Armor head;
    private Armor body;
    private Armor legs;


    //Constructor
    public Hero(HeroAttributeStrategy hero) {
        this.hero = hero;
        this.lvl = 1;
        setBaseStats();
    }

    //Set base stats, gets called from the constructor. Calls the hero strategy for base stats
    public void setBaseStats()
    {
        int[] stats = hero.getBaseStats();
        this.hp = stats[0];
        this.strength = stats[1];
        this.dexterity = stats[2];
        this.intellect = stats[3];
        this.efhp = this.hp;
        this.efstr = this.strength;
        this.efdex = this.dexterity;
        this.efint = this.intellect;
    }

    //Level up, increases hero stats. Calls the hero strategy for increasing stats.
    public void levelUp()
    {
        lvl++;
        int[] stats = hero.increaseStats();
        this.hp += stats[0];
        this.strength += stats[1];
        this.dexterity += stats[2];
        this.intellect += stats[3];
        this.xpReq = xpReq + (xpReq/10);

        /*
            Call to effectiveStats because hero can have an item equipped while lvling up.
            Compute new effective stats values.
            Otherwise, effective stats will be set to the same as basestats.
        */
        effectiveStats();
    }

    public void gainXP(int xp)
    {
        this.currentXP = xp;
        while(currentXP>=xpReq)
        {
            currentXP = currentXP - xpReq;
            levelUp();
        }
        xpReq = xpReq - currentXP;

    }

    public String equipWeapon(Weapon weapon)
    {
        if(weapon.ilvl > lvl)
        {
            return "Can't equip weapon, lvl does not meet requirement";
        }
        else
        {
            this.weapon = weapon;
            return "Weapon equipped";
        }
    }

    public String equipArmor(Armor armor)
    {
        if(armor.getIlvl() > lvl)
        {
            return "Can't equip armor, lvl does not meet requirement";
        }
        else
        {
            if(armor.getSlot().equalsIgnoreCase("body"))
            {
                this.head = armor;
            }
            if(armor.getSlot().equalsIgnoreCase("head"))
            {
                this.body = armor;
            }
            if(armor.getSlot().equalsIgnoreCase("legs"))
            {
                this.legs = armor;
            }

            effectiveStats();
            return "Armor equipped";
        }
    }

    public void effectiveStats()
    {
        //Set effective stats to base stats
        efhp = hp;
        efstr = strength;
        efdex = dexterity;
        efint = intellect;

        //add armorpiece bonuses to base stats
        if(body != null)
        {
            efhp = efhp + body.getHp();
            efstr = efstr + body.getStrength();
            efdex = efdex + body.getDexterity();
            efint = efint + body.getIntellect();
        }
        if(head != null)
        {
            efhp = efhp + head.getHp();
            efstr = efstr + head.getStrength();
            efdex = efdex + head.getDexterity();
            efint = efint + head.getIntellect();
        }
        if(legs != null)
        {
            efhp = efhp + legs.getHp();
            efstr = efstr + legs.getStrength();
            efdex = efdex + legs.getDexterity();
            efint = efint + legs.getIntellect();
        }
    }

    public int attack()
    {
        int damageDone = 0;
        if(this.weapon instanceof Melee)
        {
            damageDone = ((Melee) this.weapon).getActualDMG() + (int) (efstr * 1.5);
            return  damageDone;
        }
        else if(this.weapon instanceof Ranged)
        {
            damageDone = ((Ranged) this.weapon).getActualDMG() + (int) (efdex * 2);
            return damageDone;
        }
        else if(this.weapon instanceof Magic)
        {
            damageDone = ((Magic) this.weapon).getActualDMG() + (int) (efint *3);
            return damageDone;
        }
        else
        {
            return 0;
        }
    }

    public String getStats(){
        StringBuilder s = new StringBuilder();
        s.append("Lvl: ").append(lvl).append("\n");
        s.append("Base stats: \n");
        s.append("HP: ").append(hp).append("\n");
        s.append("STR: ").append(strength).append("\n");
        s.append("DEX: ").append(dexterity).append("\n");
        s.append("INT: ").append(intellect).append("\n");

        s.append("Effective stats: \n");
        s.append("HP: ").append(efhp).append("\n");
        s.append("STR: ").append(efstr).append("\n");
        s.append("DEX: ").append(efdex).append("\n");
        s.append("INT: ").append(efint).append("\n");
        s.append("Current XP: ").append(currentXP).append("\n");
        s.append("XP required to next level: ").append(xpReq).append("\n");
        return s.toString();
    }



}
