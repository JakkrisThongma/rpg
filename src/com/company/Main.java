package com.company;


import com.company.heroes.Hero;
import com.company.heroes.Ranger;
import com.company.heroes.Warrior;
import com.company.items.*;

public class Main {


    public static void main(String[] args)
    {
        //Ranger class
        Hero tungvekter = new Hero(new Ranger());
        System.out.println(tungvekter.getStats());


        //level up to 8
        tungvekter.gainXP(1000);

        //Create armor pieces
        Armor body = new Armor(new Leather(),"Body of the eagle","body",1);
        Armor head = new Armor(new Plate(),"Head of the eagle","head",1);
        Armor legs = new Armor(new Cloth(),"Legs of the eagle","legs",1);

        //Create all weapon types
        Ranged ranged = new Ranged("Longbow", 8);
        Melee melee = new Melee("Shortsword", 5);
        Magic magic = new Magic("Big Wand", 7);

        System.out.println(tungvekter.equipWeapon(ranged));
        System.out.println(tungvekter.equipArmor(body));
        System.out.println(tungvekter.equipArmor(head));
        System.out.println(tungvekter.equipArmor(legs));

        //print new stats
        System.out.println(tungvekter.getStats());

        Armor body2 = new Armor(new Leather(),"Body of the sparrow","body",8);
        tungvekter.equipArmor(body2);

        body2.printstats();
        head.printstats();
        legs.printstats();

        //print new stats
        System.out.println(tungvekter.getStats());

        //Attack
        System.out.println("Tungvekter attacked for: " + tungvekter.attack());
        Ranged ranged2 = new Ranged("Longbow", 1);

        tungvekter.equipWeapon(ranged2);
        System.out.println("Tungvekter attacked for: " + tungvekter.attack());
        Hero liiqa = new Hero(new Warrior());

    }
}
