package com.company.items;

public class Plate implements ArmorAttributeStrategy
{
    @Override
    public int[] getStats(int ilvl) {
        int hp = 30 + (12*ilvl);
        int strength = 3 + (2*ilvl);
        int dexterity = 1 + (1*ilvl);
        int intellect = 0;

        return new int[]{hp,strength,dexterity,intellect};
    }
}
