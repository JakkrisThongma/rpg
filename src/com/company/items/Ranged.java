package com.company.items;

public class Ranged extends Weapon {
    private int actualDMG;

    public int getActualDMG() {
        return actualDMG;
    }

    public Ranged(String name, int ilvl) {
        super(name, ilvl, "Ranged", 5);
        this.actualDMG = baseDMG + (ilvl *3);
    }
}
