package com.company.items;

public class Cloth implements ArmorAttributeStrategy
{

    @Override
    public int[] getStats(int ilvl) {
        int hp = 10 + (5*ilvl);
        int strength = 0;
        int dexterity = 1 + (1*ilvl);
        int intellect = 3 + (2*ilvl);

        return new int[]{hp,strength,dexterity,intellect};
    }
}
