package com.company.items;

public class Melee extends Weapon
{


    private int actualDMG;

    public int getActualDMG() {
        return actualDMG;
    }
    public Melee(String name, int ilvl) {
        super(name, ilvl,"Melee", 15);
        this.actualDMG = super.baseDMG +(ilvl*2);
    }



}
