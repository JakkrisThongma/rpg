package com.company.items;

public class Magic extends Weapon
{
    private int actualDMG;

    public int getActualDMG() {
        return actualDMG;
    }

    public Magic(String name, int ilvl) {
        super(name, ilvl, "Magic", 25);
        this.actualDMG = baseDMG + (ilvl * 2);
    }
}
