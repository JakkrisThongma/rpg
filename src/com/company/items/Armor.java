package com.company.items;

public class Armor
{
    public ArmorAttributeStrategy armor;
    private int hp, strength, dexterity, intellect,ilvl;
    private String name;
    private String slot;

    public Armor(ArmorAttributeStrategy armor, String name, String slot, int ilvl)
    {
        this.armor = armor;
        this.name = name;
        this.slot = slot;
        this.ilvl = ilvl;
        setStats(ilvl);
    }

    public int getHp() {
        return hp;
    }

    public int getStrength() {
        return strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public int getIntellect() {
        return intellect;
    }

    public int getIlvl() {
        return ilvl;
    }

    public String getSlot() {
        return slot;
    }

    public void setStats(int ilvl)
    {
        //Base stats from each armor type(leather, plate, cloth)
        int[]stats = armor.getStats(ilvl);

        //100 % scaling for body pieces
        if(slot.equalsIgnoreCase("body"))
        {
            this.hp = stats[0];
            this.strength = stats[1];
            this.dexterity = stats[2];
            this.intellect = stats[3];
        }

        //80 % scaling for head pieces
        if(slot.equalsIgnoreCase("head"))
        {
            this.hp = (int) (0.8 * (double) stats[0]);
            this.strength = (int) (0.8 * (double) stats[1]);
            this.dexterity = (int) (0.8 * (double) stats[2]);
            this.intellect = (int) (0.8 * (double) stats[3]);
        }

        //60 % scaling for leg pieces
        if(slot.equalsIgnoreCase("legs"))
        {
            this.hp = (int) (0.6 * (double) stats[0]);
            this.strength = (int) (0.6 * (double) stats[1]);
            this.dexterity = (int) (0.6 * (double) stats[2]);
            this.intellect = (int) (0.6 * (double) stats[3]);
        }
    }

    public void printstats()
    {
        System.out.println("Item stats for: " + name);
        System.out.println("Slot: " + slot);
        System.out.println("Bonus HP: " + hp);
        System.out.println("Bonus Str: " + strength);
        System.out.println("Bonus Dex: " + dexterity);
        System.out.println("Bonus Int: " + intellect);
        System.out.println();
    }
}
