package com.company.items;

public abstract class Weapon
{
    public String weaponType;
    public int baseDMG;
    public String name;
    public int ilvl;

    public Weapon(String name, int ilvl, String weaponType, int baseDMG) {
        this.name = name;
        this.ilvl = ilvl;
        this.weaponType = weaponType;
        this.baseDMG = baseDMG;
    }

    public String getWeaponType() {
        return weaponType;
    }
}
