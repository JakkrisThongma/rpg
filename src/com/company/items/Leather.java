package com.company.items;

public class Leather implements ArmorAttributeStrategy
{

    @Override
    public int[] getStats(int ilvl)
    {

        int hp = 20 + (8*ilvl);
        int strength = 1 + (1*ilvl);
        int dexterity = 3 + (2*ilvl);
        int intellect = 0;

        return new int[]{hp,strength,dexterity,intellect};
    }


}
