package com.company.items;

public interface ArmorAttributeStrategy
{
    int[] getStats(int ilvl);

}
